const { createLogger, transports, format } = require("winston");
require("winston-mongodb");

const logger = createLogger({
  transports: [
    new transports.File({
      filename: "logfile.log",
      level: "error"
    }),
    new transports.MongoDB({
      level: "error",
      db: "mongodb://localhost/vidly",
      options: { useUnifiedTopology: true }
    })
  ]
});

module.exports = logger;
